<?php

/**
 * @file
 * Rules integration for the Commerce Discount module.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_discount_cumulative_rules_action_info() {
  $items = array();

  $items['commerce_discount_cumulative_action'] = array(
    'label' => t('Allow or deny other discounts to be applied after this discount'),
    'group' => t('Commerce Discount'),
    'parameter' => array(
      'order' => array(
        'type' => 'commerce_order',
        'label' => t('Order')
      ),
      'commerce_discount' => array(
        'label' => t('Commerce Discount'),
        'type' => 'token',
        'options list' => 'commerce_discount_entity_list',
      ),
    ),
    'base' => 'commerce_discount_cumulative_action',
  );

  return $items;
}

/**
 * Rules action: Apply fixed amount discount.
 */
function commerce_discount_cumulative_action($order, $discount_name) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $discount_wrapper = entity_metadata_wrapper('commerce_discount', $discount_name);

  $cumulative = $discount_wrapper->commerce_discount_cumulative->value();

  if ($cumulative == 'exclusive') {
    // Remove previous discounts.
    foreach ($order_wrapper->commerce_line_items as $line_item) {
      if ($line_item->getBundle() == 'commerce_discount' && $line_item->value()->data['discount_name'] !== $discount_name) {
        commerce_line_item_delete_references($line_item->value());
        commerce_line_item_delete($line_item->line_item_id->value());
      }
    }
  }
}

/*
 * Implements hook_rules_condition_info().
 */
function commerce_discount_cumulative_rules_condition_info() {

  // Max usage per person
  $conditions['commerce_discount_cumulative_condition'] = array(
    'label' => t('Check discount can apply after another cumulative rule'),
    'parameter' => array(
      'order' => array(
        'type' => 'commerce_order',
        'label' => t('Order')
      ),
      'commerce_discount' => array(
        'label' => t('Commerce Discount'),
        'type' => 'token',
        'options list' => 'commerce_discount_entity_list',
      ),
    ),
    'group' => t('Commerce Discount'),
  );

  return $conditions;
}

/*
 * Rules condition callback: evaluate maximum usage per-person of a discount.
 */
function commerce_discount_cumulative_condition($order, $discount_name) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  // Should not add another discount if another non cumulative discount has been apply.
  foreach ($order_wrapper->commerce_discounts as $discount_wrapper) {
    if ($discount_wrapper->__isset('commerce_discount_cumulative')
      && !is_null($discount_wrapper->commerce_discount_cumulative->value())
      && $discount_wrapper->commerce_discount_cumulative->value() !== 'cumulative'
      && $discount_wrapper->name->value() != $discount_name
    ) {
      return FALSE;
    }
  }

  return TRUE;
}
